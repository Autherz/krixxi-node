var fs = require('fs')
var jwt = require('jsonwebtoken')
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    TokenType: {
      type: DataTypes.INTEGER
    },
    UserId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Token.associate = function({ User }) {
   Token.belongsTo(User)
  };
  
  Token.generate = async function(UserId, TokenType) {
    Token.destroy({ where: { UserId }});
    if (!UserId) {
      throw new Error('AuthToken requires a user ID')
    }
    
    // var privateKey = fs.readFileSync('../key/key.rsa', 'utf8')

    var token = jwt.sign({
      id: UserId,
      type: TokenType,
    }, 'krixxi')
    
    return Token.create({ token, UserId})
  }
  return Token;
};