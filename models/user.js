const bcrypt = require('bcrypt');
'use strict';
module.exports = (sequelize, DataTypes) => {

  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    signature: DataTypes.STRING,
    imageProfile: DataTypes.STRING,
    backgroundProfile: DataTypes.STRING,
    tel: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    viewer: DataTypes.INTEGER,
  }, {});
  User.associate = function(models) {
    User.hasMany(models.Token);
  };

  User.authenticate = async function(email, password) {
    const user = await User.findOne({ where: { email }})
    if (bcrypt.compareSync(password, user.password)) {
      return user.authorize();
    }

    throw new Error('invalid password')
  }

  User.Viewed =  async function(userId) {
    console.log(userId)
    await User.update(
      {viewer: sequelize.literal('viewer + 1')},
        { where: {
          id: userId
        },
      }
    ).then((result) => {
      console.log('result : ', result)
      // return res.json({message: 'update success'})
    }).catch(err => {
      console.log('err : ', err)
      // res.json(err)
    })
  }

  User.prototype.authorize = async function () {
    const user = this

    const accessToken = 1
    const refreshToken = 2
    const { Token } = sequelize.models;
    const authToken = await Token.generate(this.id, accessToken)
    const authToken2 = await Token.generate(this.id, refreshToken)
  
    await user.addToken(authToken)
    
    return { user, authToken, authToken2 }
  }

  User.prototype.logout = async function ( token) {
    sequelize.models.AuthToken.destroy({ where: { token }});
  }
  return User;
};