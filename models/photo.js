'use strict';
module.exports = (sequelize, DataTypes) => {
  const Photo = sequelize.define('Photo', {
    fieldname: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    originalname: {
        type: DataTypes.STRING,
    },
    encoding: {
      type: DataTypes.STRING,
    },
    mimetype: {
      type: DataTypes.STRING,
    },
    destination: {
      type: DataTypes.STRING,
    },
    filename: {
      type: DataTypes.STRING,
    },
    path: {
        type: DataTypes.STRING,
    },
    size: {
      type: DataTypes.INTEGER
    },
    view: {
        type: DataTypes.INTEGER,
    },
    UserId: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    AlbumId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {});
  Photo.associate = function({ Album }) {
    Photo.belongsTo(Album)
  };

  Photo.associate = function({ User }) {
      Photo.belongsTo(User)
  }

  Photo.generate = async function (Photos, AlbumId, UserId) {
    console.log("photo generate")
    var result = Photos.map(photo => {
      var o = Object.assign({}, photo, { destination: photo.destination.replace('./public', '')}, { path: photo.path.replace('public', '')}, {view: 0})
      o.UserId = UserId
      o.AlbumId = AlbumId
      return o
    })
    Photo.bulkCreate(result)
    return result
  }

  return Photo;
};