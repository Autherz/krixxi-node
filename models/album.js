'use strict';
module.exports = (sequelize, DataTypes) => {
  const Album = sequelize.define('Album', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
        type: DataTypes.STRING,
    },
    view: {
        type: DataTypes.INTEGER,
    },
    UserId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Album.associate = function({ User }) {
    Album.belongsTo(User)
  };

  return Album;
};