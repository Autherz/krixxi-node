require('dotenv').config()


module.exports = {
  "development" : {
    "database" : process.env.DEV_DATABASE,
    "username" : process.env.DEV_USERNAME,
    "password" : process.env.DEV_PASSWORD,
    "host" : process.env.DEV_HOST,
    "dialect" : "mysql"
  },

  "test" : {
    "database" : process.env.POSTGRES_DATABASE,
    "username" : process.env.POSTGRES_USERNAME,
    "password" : process.env.POSTGRES_PASSWORD,
    "host" : process.env.POSTGRES_HOST,
    "dialect" : "postgres"
  },

  "production" : {
    "database" : process.env.PROD_DATABASE,
    "username" : process.env.PROD_USERNAME,
    "password" : process.env.PROD_PASSWORD,
    "host" : process.env.PROD_HOST,
    "dialect" : "mysql"
  }
}

// {
//   "development": {
//     "username": "root",
//     "password": "emvpteam",
//     "database": "krixxi",
//     "host": "127.0.0.1",
//     "dialect": "mysql",
//     "operatorsAliases": false
//   },
//   "test": {
//     "username": "root",
//     "password": null,
//     "database": "database_test",
//     "host": "127.0.0.1",
//     "dialect": "mysql",
//     "operatorsAliases": false
//   },
//   "production": {
//     "username": "root",
//     "password": null,
//     "database": "database_production",
//     "host": "127.0.0.1",
//     "dialect": "mysql",
//     "operatorsAliases": false
//   }
// }
