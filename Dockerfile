FROM node:10.15.0

RUN mkdir -p /node
WORKDIR /node

RUN npm install -g nodemon
RUN npm config set registry https://registry.npmjs.org

# COPY package.json /node/package.json
# RUN npm install \
#     && npm ls \
#     && npm cache clean --force \
#     && mv /node/node_modules /node_modules

COPY . /node

RUN npm install

ENV PORT 3000
EXPOSE 3000

# cmd ["node", "server.js"]

    
