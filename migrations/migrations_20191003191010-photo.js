'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Photos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fieldname: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      originalname: {
          type: Sequelize.STRING,
      },
      encoding: {
        type: Sequelize.STRING,
      },
      mimetype: {
        type: Sequelize.STRING,
      },
      destination: {
        type: Sequelize.STRING,
      },
      filename: {
        type: Sequelize.STRING,
      },
      path: {
          type: Sequelize.STRING,
      },
      size: {
        type: Sequelize.INTEGER
      },
      view: {
          type: Sequelize.INTEGER,
      },
    
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      UserId: {
        type: Sequelize.STRING,
        onDelete: "CASCADE",
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      AlbumId: {
        type: Sequelize.STRING,
        onDelete: "CASCADE",
        allowNull: false,
        references: {
          model: 'Albums',
          key: 'id'
        }
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
   return queryInterface.dropTable('Photos');
  }
};
