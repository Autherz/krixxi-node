var express = require('express');
var router = express.Router();
var models = require('../models')
const bcrypt = require('bcrypt')
const customAuthMiddleware = require('../middleware/custom-auth-middleware')
/* GET users listing. */

const preCreatePassSave = (req, res, next) => {
  try {
    if (req.body.password) {
      req.body.password = bcrypt.hashSync(req.body.password, 12)
    }
    next()
  } catch (_err) {
    next(_err)
  }
}

function isIdUnique (email) {
  return models.User.count({ where: { email: email} })
    .then(count => {
      if (count != 0) {
        return false;
      }
      return true;
    });
}

router.post('/', preCreatePassSave,function(req, res) {
  console.log(req.body)
  isIdUnique(req.body.email).then(isUnique => {
    if (isUnique) {
      models.User.create({
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        tel: req.body.tel,
      }).then(function() {
        res.status(200).json({message: 'create user success'})
      });

    } else {
      res.status(400).json({message: 'Email is already exist !'})
    }
  })
  
});

router.post('/register', async (req, res) => {
  const hash = bcrypt.hashSync(req.body.password, 12);
  var ts = Math.round((new Date()).getTime() / 1000);

  isIdUnique(req.body.email).then(async isUnique => {
    if (isUnique) {
      try {
        let user = await models.User.create(
          Object.assign({id: 'pg-'+ ts }, req.body, {password: hash}, {imageProfile: 'images/default_avatar.png'}, {backgroundProfile: 'banner/profile-banner.jpg'}, {active: false}, {viewer: 0})
        );
        
        // generate token and save to db 
        let data = await user.authorize();
        return res.json(data);
      }catch (err) {
        return res.status(400).send(err)
      }
    } else {
      res.status(400).json({message: 'Email is already exist !'})
    }
  })
})

router.get('/', async (req, res) => {
  try {
    let user = await models.User.findAll({
      where: {
        active: true
      }
    })
    return res.json(user)
  }catch(err) {
    return res.status(400).send(err)
  }
})

router.put('/:id',customAuthMiddleware , async (req, res) => {

  models.User.update(
    Object.assign({}, req.body),
      { where: {
        id: req.params.id
      },
    }
  ).then((result) => {
    return res.json({message: 'update success'})
  }).catch(err => {
    return res.json(err)
  })
})

router.post('/profile',customAuthMiddleware ,async (req, res) => {

  
  try {
    let user = await models.User.findAll({ where: {
      id: req.user.id
    }})
    return res.json(user)
  } catch (err) {
    return res.status(400).send(err)
  }
})

router.get('/:id', async (req, res) => {
  models.User.Viewed(req.params.id)
  try {
    let user = await models.User.findOne({ where: {
      id: req.params.id
    }})
    return res.json(user)
  }catch(err) {
    return res.status(400).send(err)
  }
})

router.get('/pg/recommended', async ( req, res) => {
  try {
    let user = await models.User.findAll({
      where: {
        active: true
      },
      order: [
        ['viewer', 'DESC']
      ]
    })

    return res.json(user)
  } catch (err) {
    return res.json(err)
  }
})


router.get('/:user_id/destroy', function(req, res) {
  models.User.destroy({
    where: {
      id: req.params.user_id
    }
  }).then(function() {
    res.redirect('/');
  })
})

router.post('/:user_id/task/create', function(req, res) {
  models.Task.create({
    title: req.body.title,
    UserId: req.params.user_id
  }).then(function() {
    res.redirect('/')
  })
})

router.get('/:user_id/tasks/:task_id/destroy', function(req, res){
  models.Task.destroy({
    where: {
      id: req.params.task_id
    }
  }).then(function() {
    res.redirect('/')
  })
})
module.exports = router;
