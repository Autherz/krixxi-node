var express = require('express')
var router = express.Router();
var models = require('../models')
var customAuthMiddleware = require('../middleware/custom-auth-middleware')

router.get('/:id', async(req, res) => {

    try {
        let photos = await models.Photo.findAll({
            where: {
                AlbumId: req.params.id
            }
        })
        res.json(photos)
    } catch (err) {
        res.json(err)
    }
})

module.exports = router;