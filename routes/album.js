var express = require('express')
var router = express.Router();
var models = require('../models')
var upload = require('../middleware/upload-middleware')
var customAuthMiddleware = require('../middleware/custom-auth-middleware')

router.post('/upload', customAuthMiddleware, upload.array('photos'), async(req, res) => {

    var ts = Math.round((new Date()).getTime() / 1000);
    var albumId = 'album-' + ts
        // console.log(req.body)
        // console.log(req.files)
    try {
        if (req.files) {
            // delete req.body.photos
            let album = await models.Album.create(
                Object.assign({ id: albumId }, req.body, { view: 0 }, { UserId: req.user.id })
            )

            // res.json(req.files)
            let photos = await models.Photo.generate(req.files, albumId, req.user.id)

            return res.json({ album, photos })
        }
        res.json({ message: 'No file image!' })
            // else throw 'error';
    } catch (err) {
        res.json(err)
    }
})

router.get('/', async(req, res) => {
    try {
        let album = await models.Album.findAll({
            // where: {
            //     active: true,
            // },
            include: [{
                model: models.User,
                where: {
                    active: true
                }
            }]
        })
        res.json(album)
    } catch (err) {
        res.json(err)
    }
})

router.get('/:id', async(req, res) => {
    try {
        let album = await models.Album.findAll({
            where: {
                UserId: req.params.id
            }
        })
        res.json(album)
    } catch (err) {
        res.json(err)
    }
})


module.exports = router;