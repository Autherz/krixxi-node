const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
var express = require('express');
var router = express.Router();
var models = require('../models')
const customAuthMiddleware = require('../middleware/custom-auth-middleware')

router.post('/auth', async function(req, res, next) {
    const { email, password, grant_type } = req.body

    if (!email, !password) {
        return res.status(400).send(
            'Request missing email or password param'
        );
    }

    if (grant_type == "password") {
        try {
            let user = await models.User.authenticate(email, password)

            // user = await user.authorize();
            // res.cookie('auth_token', user.authToken.token, {signed:true})
            return res.json({
                refreshToken: user.authToken.token,
                accessToken: user.authToken2.token
            })
        } catch (err) {
            console.log(err)
            return res.status(400).send('invalid email or password')
        }
    }
    
});

router.delete('/logout', async (req, res) => {
    const {user, cookies: { auth_token: authToken}} = req

    if (user && authToken) {
        await req.user.logout(authToken)
        return res.status(204).send()
    }

    return res.status(400).send(
        { errors: [{message: 'not authenticated'}]}
    )
})

router.get('/me', customAuthMiddleware, (req, res) => {
    if (req.user) {
        return res.json({
            id: req.user.id,
            signature: req.user.signature
        })
    }
    res.status(404).send(
        {errors: [{message: 'missing auth token'}]}
    )
})

module.exports = router;