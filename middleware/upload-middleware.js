var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads')
    },
    filename: function (req, file, callback) {
        callback(null,file.fieldname + '-' + Date.now() + '.jpg')
    }
})

const fileFilter = (req, file, callback) => {
    const allowedTypes = ["image/jpeg", "image/jpg", "image/png"]
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file")
        error.code = "INCORRECT_FILETYPE"
        return callback(error, false)
    }
    callback(null, true)
}

const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 4 * 1024 * 1024,
    }
})

module.exports = upload