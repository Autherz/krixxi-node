const models = require('../models')
const jwt = require('jsonwebtoken')
module.exports = async function(req, res, next) {
    // const token = req.signedCookies.auth_token || req.headers.authorization;
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (token) {

            jwt.verify(token, 'krixxi', async (_err, payload) => {
                if(payload) {
                    req.user = payload
                }
            })
            // const authToken = await models.Token.findOne(
            //     { where: { token }, include: models.User }
            // )

            // if (authToken) {
            //     req.user = authToken.User
            // }
        }
        next();
    } catch (err) {
        res.status(401).json(err)
    }
    
}